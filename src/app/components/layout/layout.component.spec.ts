import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatBottomSheet } from "@angular/material";
import { LayoutComponent } from "./layout.component";
import { RouterTestingModule } from "@angular/router/testing";
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { Overlay } from "@angular/cdk/overlay";
import { ActionsComponent } from "../actions/actions.component";

class MatBottomSheetStub {
  open() {}
}

fdescribe("LayoutComponent", () => {
  let component: LayoutComponent;
  let fixture: ComponentFixture<LayoutComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [LayoutComponent],
        providers: [
          Overlay,
          { provide: MatBottomSheet, useCLass: MatBottomSheetStub },
        ],
        imports: [
          RouterTestingModule.withRoutes([
            {
              path: "",
              component: LayoutComponent,
            },
            {
              path: "app/add",
              component: LayoutComponent,
            },
          ]),
        ],
        schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set the editMode to false", () => {
    const verifyEditMode = spyOn(component, "verifyEditMode").and.callThrough();

    fixture.ngZone.run(() => {
      //Se utiliza el (<any>component) cuando son privados
      (<any>component).router.navigate(["app/add"]);

      // esperar que la aplicación este estable
      fixture.whenStable().then(() => {
        expect(component.editMode).toBeTruthy();
        expect(verifyEditMode).toHaveBeenCalled();
      });
    });
  });

  it("should set the editMode to true", () => {
    const verifyEditMode = spyOn(component, "verifyEditMode").and.callThrough();

    fixture.ngZone.run(() => {
      //Se utiliza el (<any>component) cuando son privados
      (<any>component).router.navigate(["/"]);

      // esperar que la aplicación este estable
      fixture.whenStable().then(() => {
        expect(component.editMode).toBeFalsy();
        expect(verifyEditMode).toHaveBeenCalled();
      });
    });
  });

  it("should open", () => {
    const open = spyOn((<any>component).bottomSheet, "open");
    component.openBottomSheet();

    expect(open).toHaveBeenCalledWith(ActionsComponent);
  });
});
