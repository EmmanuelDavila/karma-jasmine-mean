import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormComponent } from "./form.component";
import { RepositoryService } from "../../services/repository.service";
import { of } from "rxjs";
import { MatSnackBar } from "@angular/material";
import { NavigationService } from "src/app/services/navigation.service";
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { FormArray, ReactiveFormsModule } from "@angular/forms";

class RepositoryServiceStub {
  savePins() {
    return of(true);
  }
}

class NavigationServiceStub {
  goToPins() {}
}

class MatSnackBarStub {
  open() {
    return {
      afterDismissed: () => {
        return of(true);
      },
    };
  }
}

fdescribe("FormComponent", () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [FormComponent],
        providers: [
          { provide: RepositoryService, useClass: RepositoryServiceStub },
          { provide: NavigationService, useClass: NavigationServiceStub },
          { provide: MatSnackBar, useClass: MatSnackBarStub },
        ],
        schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
        imports: [ReactiveFormsModule],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  fdescribe("When component is initialized", () => {
    it("Should create the firstForm", () => {
      // console.log(Object.keys(component.firstFormGroup.controls));
      expect(Object.keys(component.firstFormGroup.controls)).toEqual([
        "title",
        "author",
        "description",
      ]);
    });

    it("Should create the secondForm ", () => {
      expect(Object.keys(component.secondFormGroup.controls)).toEqual([
        "firstAsset",
        "assets",
      ]);
    });
  });

  fdescribe("When add Asset is executed", () => {
    it("should add new group", () => {
      const assets = <FormArray>component.secondFormGroup.get("assets");

      component.addAsset();
      component.addAsset();
      console.log(Object.keys(assets.controls));

      expect(Object.keys(assets.controls)).toEqual(["0", "1"]);
    });
  });

  fdescribe("When deleteAssest", () => {
    it("should remove the form control", () => {
      const assets = <FormArray>component.secondFormGroup.get("assets");

      component.addAsset();
      component.deleteAsset(assets);

      expect(Object.keys(assets.controls)).toEqual([]);
    });
  });

  fdescribe('When savePins is executed', () => {
    it('Should navigate to pins view', () => {
      const navigate = spyOn((<any>component).navigate, 'goToPins');
      const open = spyOn((<any>component).snackBar, 'open').and.callThrough();

      component.savePin();

      expect(navigate).toHaveBeenCalled();
      console.log('working');
      expect(open).toHaveBeenCalledWith('Your pin is saved, Redirecting ...', 'Cool!', {
        duration: 2000
      });
    });
  });
});
