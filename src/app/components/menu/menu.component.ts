import { EventEmitter } from "@angular/core";
import { Component, Output } from "@angular/core";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
})
export class MenuComponent {
  @Output() clicked = new EventEmitter<boolean>();
  counter: number = 0;

  incrementar() {
    this.counter++;
  }
}
