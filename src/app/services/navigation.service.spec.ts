import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NavigationService } from "./navigation.service";
import { ApiService } from "./api.service";

fdescribe("NavigationService", () => {
  let service: ApiService;
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [ApiService],
      imports: [HttpClientTestingModule],
    })
  );

  beforeEach(() => {
    service = TestBed.get(ApiService);
  });
  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
